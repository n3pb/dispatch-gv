# dispatch-gv

## What is this?

This is an initial draft of a document on using Google Voice as the primary
phone number for a rescue organization.  This document is viewable to the
public so it should not contain confidential information.  Once the initial
draft is complete, it will be moved into the domain of the organization.

## Testing ROE

* Please do not test things that generate notifications outside of
  0800-2200.  We can certainly change the hours if this doesn't work
  for someone.
* Please do experiment with sending/receiving SMS, calls, and voicemail.
  We need to see if this will really work for dispatchers.

## Getting Started

* Install the Google Voice app on your phone and add the tr-dispatch-phone account.
    * You probably want to make sure you have turned off sync for all
      but contacts for this account.
    * You probably want to turn off chat notifications for this account.
      This may already be set for all users.
* Make sure you are a member of the test-dispatch group.
* Login to the tr-dispatch-phone account in Google Voice in a browser.
    * Here you can view and send SMS, listen to voicemail (or read transcripts),
      and originate and answer calls if you have a headset.
* Add the tr-dispatch-phone to your personal contacts.
* Add your info to tr-dispatch-phone's contacts so test calls are identified.
* If you have a landline, add it as a linked number in the tr-dispatch-phone
  account.
    * You can have dispatch calls ring your landline and originate calls via
      the web interface.

## Issues to be Considered

What are the big questions at the moment?

### Answer versus Voicemail

### Notifications from Google Voice

Google sometimes tries to be smart about suppressing duplicate notifications
when a client is logged in multiple times, e.g. browser and phone app.  It
is not 100% known how things will work with multiple dispatchers running
the app.

### Initial Alert versus Incident Communication

If we have notifications

### External Services

#### SWAN

#### Other

## Using Google Voice

### The Phone App

* Like most Google apps, you can be logged into several accounts at once.
  Login to the tr-dispatch-phone app user in the app.
* If you use Google Voice from another account, be mindful of which account
  you are using when you are using the app.

### Web Browser

* You can originate and receive calls if you have a microphone and speaker.
* You can originate and receive SMS messages.
* You can play voicemails.
* You can change settings for the account.  For most users, this will only
  be to add a linked number.
* Will generate notifications for calls and SMS.

### Linked Numbers

Google Voice allows you to add "linked numbers" which are (mobile or POTS)
numbers which receive calls (and SMS messages in the case of mobile).

* A particular number can only be linked to one Google Voice account.
* Linked numbers are mostly useful for land lines, i.e. cases where you
  can't run the GV app.
* A linked number can be used for a call originated in a web browser.
* Caller ID for incoming calls can be set to the Google Voice number
  rather than the originating number.  This is the current setting and
  may provide a good way to identify calls that should be answered.

## Google Domain Accounts, Groups, and Chats

Stuff that is in the organization's Google domain.

### Account: tr-dispatch-phone

* Owns the Google Voice number.
* Password is shared with dispatchers so they are able to add the account
  to their phone in the Google Voice app or use it in a web browser.
* Except for Google Voice and possibly other alert sources, this account is
  not intended to be used to originate or receive email.

### Group: tr-dispatch-phone-g

This is a Google group for distributing mail from Google Voice
to the phone account.

* Domain-level forwarding for the tr-dispatch-phone account is set to this
  group, but only for external messages.  This eliminates the annoyance of
  messages sent to everyone being forwarded.
* This group adds a label TR-Dispatch-Phone on the subject of the messages
  posted.
* The primary group member is the dispatch group.  In this case, the
  main function of the tr-dispatch-phone group is just to add the subject label.
* This group can also be used to forward messages to external notification
  systems or mailboxes of dispatchers that get more attention than their
  organizational mailbox.
* Note: This level of indirection is required because the domain does
  not permit a user to set forwarding or use filters that forward.

### Group: test-dispatch

This is a Google group that serves as a proxy for the main dispatch group
during testing.

* Messages include messages forwarded from Google Voice which will eventually
  be forwarded to the main dispatch group.
* Messages also include discussion of how we might use the number as the
  primary dispatch number.

### Chat: dispatch

The dispatchers currently use this group to figure out who is going to handle
a particular incident.

* Use of this group will probably not change beyond the phone providing another
  channel to initiate an incident.
* It may be possible to integrate notificatinos from Voice into this group.
  This is left as a future project.
